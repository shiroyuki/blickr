var gulp = require('gulp'),
    exec = require('child_process').exec
;

gulp.task('default', function() {
    console.log('Run "watch" command instead.');
});

var paths = {
    jsx: ['Makefile', 'src/demo.js', 'src/js/**.js'],
    scss: ['Makefile', 'src/scss/**.scss']
};

gulp.task('compile.jsx', function () {
    exec('make build', function (error, stdout, stderr) {});
});

gulp.task('compile.scss', function () {
    exec('make css', function (error, stdout, stderr) {});
});

// Rerun the task when a file changes
gulp.task('watch', function() {
    gulp.watch(paths.jsx, ['compile.jsx']);
    gulp.watch(paths.scss, ['compile.scss']);
});