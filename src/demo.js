x = BlickrController.mount(document.getElementById('content'));
sample = [
    {
        type: 'table',
        value: [
            ['Teacher', 'John Doe'],
            ['Availability', 'Monday - Wednesday, Saturday'],
        ]
    },
    {
        type: 'section',
        value: 'abc'
    },
    {
        type: 'table',
        value: [
            ['Week', '1', '2', '3', '4'],
            ['Level 1', 'Panda'],
            ['Level 2', null, 'Google'],
        ]
    },
    {
        type: 'section',
        value: 'def'
    },
    {
        type: 'section',
        value: 'ghi'
    },
    {
        type: 'section',
        value: 'jkl'
    },
];
