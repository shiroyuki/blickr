function BlickrInserter() {
    // ...
}

extendComponent(BlickrInserter, {
    classes: [
        'tool',
        'inserter'
    ],

    template: function () {
        return [
            '<div>',
                '<a class="toggler"></a>',
                '<a class="new-element" data-type="section">Section</a>',
                //'<a class="new-element" data-type="image">Image</a>',
                '<a class="new-element" data-type="table">Table</a>',
            '</div>'
        ].join('');
    },

    activate: function () {
        this.context.addClass('active');
    },

    deactivate: function () {
        this.context.removeClass('active');
    },

    ready: function () {
        this.$.document.on('click', $.proxy(this.onBlur, this));
        this.context.find('.toggler').on('click', $.proxy(this.onToggle, this));
        this.context.find('.new-element').on('click', $.proxy(this.onClickNewElement, this));
    },

    onToggle: function (e) {
        e.stopPropagation();

        this.context.toggleClass('active');

        this.fire(
            'activation.toggle',
            {
                active:    this.context.hasClass('active'),
                reference: this
            }
        );
    },

    onBlur: function (e) {
        this.context.removeClass('active');
    },

    onClickNewElement: function (e) {
        var type = e.currentTarget.getAttribute('data-type');

        this.fire('insert', {
            type: type,
            reference: this
        });
    }
});