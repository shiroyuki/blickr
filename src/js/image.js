function BlickrImage() {
    // ...
}

extendComponent(BlickrImage, {
    classes: [
        'element',
        'image'
    ],

    template: function () {
        return [
            '<div data-url="">',
                '<img src>',
                '<a class="trigger url">Set the image URL</a>',
            '</div>',
        ].join('');
    },

    // on-dragenter="{{ onDragOver }}"
    //         on-dragover="{{ onDragOver }}"
    //         on-dragend="{{ onDragEnd }}"
    //         on-dragleave="{{ onDragEnd }}"
    //         on-drop="{{ onDrop }}"

    ready: function () {
        this.context.setAttribute('data-drag', false);

        this.on('dragenter', $.proxy(this.onDragOver, this))
            .on('dragover',  $.proxy(this.onDragOver, this))
            .on('dragend',   $.proxy(this.onDragEnd,  this))
            .on('dragleave', $.proxy(this.onDragEnd,  this))
            .on('drop',      $.proxy(this.onDrop,     this))
        ;
    },

    onDragOver: function (e) {
        e.preventDefault();

        this.context.setAttribute('data-drag', true);
    },

    onDragEnd: function (e) {
        e.preventDefault();

        this.context.setAttribute('data-drag', false);
    },

    onDrop: function (e) {
        var files = e.dataTransfer.files;

        e.preventDefault();

        this.context.setAttribute('data-drag', false);

        // Handle uploading
        // @see http://html5doctor.com/drag-and-drop-to-server/

        this.fire('upload.image', files);

        return false;
    }
});