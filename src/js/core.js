// Container
function BlickrContainer() {
    this.guid    = this._lastCUID++;
    this.context = null;
}

extendComponent(BlickrContainer, {
    _lastCUID: 0,

    _componentMap: {
        section:  BlickrSection,
        inserter: BlickrInserter,
        //image:    BlickrImage,
        table:    BlickrTable,
    },

    _subTemplateAddons: '<div class="addons"><a class="addon trigger element-remover" title="Remove this element."></a></div>',

    classes: [
        'blickr',
        'core',
    ],

    restore: function (serializedElements) {
        var se, // serialized element
            e,  // blickr element
            i,
            tas, // textareas
            l = serializedElements.length
        ;

        this.context.empty();

        for (i = 0; i < l; i++) {
            se = serializedElements[i];
            e  = this.addElement(se.type);

            e.load(se.value);
        }

        this.context.find('textarea').trigger('input');
    },

    val: function () {
        var sequence = [];

        this.context.children('.element').each(function (i) {
            sequence.push(this.controller.val());
        });

        return sequence;
    },

    template: function () {
        return '<div class="blickr" data-guid="{{ id }}"></div>';
    },

    addElement: function (type, reference) {
        var element, inserter;

        element  = this.add(type, reference);
        inserter = this.add('inserter', element);

        // Bind events for a new inserter.
        inserter.on('insert',            $.proxy(this.onInserterInsert, this));
        inserter.on('activation.toggle', $.proxy(this.onInserterActivationToggle, this));

        // Inject the element remover.
        element.context.children().eq(0).before(this._subTemplateAddons);

        this.fire('structure.change', null);

        return element;
    },

    add: function (type, reference) {
        var component, inserter;

        if (!this._componentMap[type]) {
            throw 'shiroyuki.blickr.UnknownComponent';
        }

        component = new (this._componentMap[type])();

        if (reference) {
            component.mount(reference.context, 'insertAfter');

            return component;
        }

        component.mount(this.context, 'appendTo');

        return component;
    },

    ready: function () {
        // DOM Event: Action: Remove the entire element (sub-component/ancestor container).
        this.context.on(
            'click',
            '.trigger.element-remover',
            $.proxy(this.onClickRemoveElement, this)
        );

        // DOM Event: Action: Automatically resize all textarea's like Facebook input.
        this.context.on(
            'input',
            'textarea',
            $.proxy(this.onChangeTextareaResize, this)
        );

        // DOM Event: Action: Disable highlighting.
        this.context.on(
            'click',
            '.trigger, .toggler',
            this.onClickDisableAccidentalHighlighting
        );

        // DOM Event: Action: Disable highlighting.
        this.context.on(
            'mousedown',
            '.trigger, .toggler',
            this.onClickDisableAccidentalHighlighting
        );

        // Local Event: Action: Update the element count.
        this.on(
            'structure.change',
            $.proxy(this.onStructuralChange, this)
        );

        // Add the default element (section).
        this.addElement('section');
    },

    onStructuralChange: function (e) {
        this.context.attr('data-element-count', this.context.children('.element').length);
    },

    onInserterInsert: function (e) {
        this.addElement(e.detail.type, e.detail.reference);
    },

    onInserterActivationToggle: function (e) {
        this.context.find('.tool.inserter').each(function (index) {
            if (e.detail.reference === this.controller) {
                return;
            }

            this.controller.deactivate();
        });
    },

    onChangeTextareaResize: function (e) {
        var element = e.currentTarget,
            context = $(element),
            previousHeight = context.innerHeight()
        ;

        context
            .css({'height':'auto'})
            .height(element.scrollHeight);
    },

    onClickDisableAccidentalHighlighting: function (e) {
        e.preventDefault();
    },

    onClickRemoveElement: function (e) {
        var target           = $(e.currentTarget).closest('.element'),
            collateralDamage = target.next()
        ;

        e.preventDefault();

        collateralDamage.remove();
        target.remove();

        this.fire('structure.change', null);
    }
});

