CURRENT_DIR=`pwd`
JS_TARGETS=util,inserter,image,section,table,core

SOURCE_PATH=src
BUILD_PATH=build

JS_SOURCE_PATH=$(SOURCE_PATH)/js
JS_BUILD_PATH=$(BUILD_PATH)/js
JS_COMBINED_PATH=$(SOURCE_PATH)/blickr.js

SCSS_PATH=$(SOURCE_PATH)/scss
CSS_PATH=$(BUILD_PATH)/css

MINIFIER=node_modules/uglify/bin/uglify
MINIFY_JS=$(MINIFIER) -s $(JS_BUILD_PATH)/blickr.js -o $(JS_BUILD_PATH)/blickr.min.js

# Make the production build
build: assemble css
	@#jsx $(SOURCE_PATH)/ $(JS_BUILD_PATH)/
	@mkdir -p $(JS_BUILD_PATH)/
	@cp -f $(JS_COMBINED_PATH) $(JS_BUILD_PATH)/
	@$(MINIFY_JS)
	@echo 'Code compiled'

assemble: clean
	cat $(JS_SOURCE_PATH)/{$(JS_TARGETS)}.js > $(JS_COMBINED_PATH)
	@echo 'Code assembled'

clean:
	rm -rf $(JS_COMBINED_PATH) $(JS_BUILD_PATH) 2> /dev/null; echo 'Build cache removed'

# Make the production build for CSS
css:
	@sass --update $(SCSS_PATH):$(CSS_PATH) --style compressed

# Auto-make the dev build for CSS
css_live:
	@sass --watch $(SCSS_PATH):$(CSS_PATH) --style compressed

# Auto-make everything (production level) with Task Runner
live_update:
	@node_modules/gulp/bin/gulp.js watch