# Blickr

This is an experimental content editor inspired by Medium.

Tested with jQuery 2.1.

## Installation

To install Blickr, in the Bower config file, add:

    "blickr": "git@bitbucket.org:shiroyuki/blickr.git"

to the list of dependencies.

## Usage with little sample

To use Blickr, first, add the following into the HTML code:

    <link rel="stylesheet" href="<BOWER_COMPONENTS>/blickr/build/css/blickr.css">
    <script src="<BOWER_COMPONENTS>/jquery/dist/jquery.min.js"></script>
    <script src="<BOWER_COMPONENTS>/showdown/src/showdown.js"></script>
    <script src="<BOWER_COMPONENTS>/blickr/build/js/blickr.js"></script>

Then, **mount (append)** the custom element (the content editor) like the following example:

    container     = document.querySelector('#container');
    blickrComponent = BlickrController.mount(container);

You will get the structed value of the custom element by either:

    blickrComponent.val() // @return array<object>

or

    blickrElement = container.querySelector('.blickr');
    blickrElement.controller.val() // @return array<object>

## Public APIs

### BlickrController

This is the main controller to generate **BlickrContainer**.

#### function mount(reference: jQuery|DOMElement) -> BlickrContainer

To mount (append) an element to the reference element.

### BlickrContainer

This is a container of the custom component.

#### function val() -> array<object>

Retrieve the content (structed).

#### function addElement() -> (BlickrInternalComponent)

(Internal API) Add an internal (sub) component.